require 'pg'

$conn = PG.connect :dbname => 'statistics', :user => 'postgres', :password => 'password'

rg = ['cat','hat','man','van','Dad','mad','cap','map','net','wet','pen','ten','bed','red','leg','peg','six','mix','dog','log','hop','mop','fox','box','pot','dot','jug','mug','run','sun','cup','up','cut','hut','drip','skip','plum','drum','grin','twin','sand','hand','camp','lamp','nest','rest','tent','bent','fist','list','jump','bump','ship','chip','fish','dish','chick','lick','clap','snap','flag','drag','stop','drop','plug','slug','chimp','limp']

def random(array)
  array.sample(6).each do |word|
    i = $conn.query("SELECT count FROM words WHERE word = '#{word}'").field_values('count')
    $conn.query("UPDATE words SET count = #{i[0].to_i}+1 WHERE word = '#{word}'")
  end
end


t1 = Time.now
100000.times {random(rg)}
t2 = Time.now
puts "Script was running #{t2-t1} sec"

results = $conn.query( "SELECT word, count FROM words ORDER BY count DESC" ).values
