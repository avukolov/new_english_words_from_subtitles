﻿1
00:00:14,814 --> 00:00:16,111
Hello. Back already?

2
00:00:16,282 --> 00:00:20,616
That episode is what we refer
to as a cliffhanger.

3
00:00:20,787 --> 00:00:24,689
To find out who Cartman's father was, we
told viewers to tune in four weeks later.

4
00:00:24,858 --> 00:00:28,294
Four weeks later was April Fools'
and we thought it would be fun...

5
00:00:28,461 --> 00:00:32,420
...to not air the cliffhanger payoff,
but air something irrelevant.

6
00:00:32,599 --> 00:00:37,002
<i>- A full Terrance & Phillip episode.
- The viewers responded splendidly.</i>

7
00:00:37,170 --> 00:00:40,162
We saved their comments
on our answering machine.

8
00:00:40,340 --> 00:00:44,606
<i>I'm gonna cut off your testicles.
I'm never watching South Park again!</i>

9
00:00:44,778 --> 00:00:48,942
<i>Think you're funny not telling us who
Cartman's father is? I hope you die.</i>

10
00:00:49,115 --> 00:00:54,280
<i>I can't go on not knowing who
Cartman's father is. I just can't. I can't go-</i>

11
00:00:54,454 --> 00:00:59,016
- Now can I have my medication?
- Dude, do the dog dance some more.

12
00:00:59,659 --> 00:01:01,684
Yeah. Beep like a jeep.

13
00:01:01,861 --> 00:01:03,795
Do puffer fish.

14
00:01:03,963 --> 00:01:05,453
I'll swim. Puffer fish.

15
00:01:05,632 --> 00:01:08,829
You want your medicine?
Here. Go get it.

16
00:01:09,002 --> 00:01:12,529
Oh, no.
You guys are jerks.

17
00:01:13,273 --> 00:01:14,740
- Hello.
- What a grump.

18
00:01:14,908 --> 00:01:16,705
Jeez, Mr. Grumpy Pants.

19
00:01:16,876 --> 00:01:19,640
<i>After the Terrance & Phillip
episode...</i>

20
00:01:19,813 --> 00:01:22,873
...we aired the episode
you're about to see.

21
00:01:23,049 --> 00:01:27,008
It's called
"Cartman's Mom is Still a Dirty Slut."

22
00:01:28,188 --> 00:01:29,712
<i>Previously on South Park:</i>

23
00:01:29,889 --> 00:01:32,722
<i>Sobriety fills the laboratory
as the men of South Park...</i>

24
00:01:32,892 --> 00:01:35,417
<i>...gather to find out
which one fathered this boy.</i>

25
00:01:35,595 --> 00:01:37,654
<i>Who is Eric Cartman's father?</i>

26
00:01:37,831 --> 00:01:40,857
<i>At the end of tonight's episode,
you will know.</i>

27
00:02:12,332 --> 00:02:13,890
Cartman's Mom is Still A Dirty Slut

28
00:02:15,935 --> 00:02:17,197
Now, to continue.

29
00:02:17,370 --> 00:02:23,775
- The father is someone in this room.
- This is the longest minute of my life.

30
00:02:23,943 --> 00:02:25,035
Hey, Kenny.

31
00:02:25,211 --> 00:02:28,237
Gentlemen, the father is...

32
00:02:28,414 --> 00:02:33,044
- Hey, what the hell's going on?
- It's a power outage.

33
00:02:34,854 --> 00:02:36,082
Is everybody okay?

34
00:02:36,256 --> 00:02:39,089
- That sounded like a gunshot.
- Oh, my God! Look!

35
00:02:40,593 --> 00:02:42,652
Oh, my God! They killed Mephisto.

36
00:02:42,862 --> 00:02:45,797
- You bastards!
- Mephisto's been shot!

37
00:02:45,965 --> 00:02:49,230
- Is he dead?
- Hey, this window is shot out too.

38
00:02:49,402 --> 00:02:51,996
That means the killer
was not in this room.

39
00:02:52,172 --> 00:02:54,800
<i>- Then who was it?
- Who shot Mephisto?</i>

40
00:02:54,974 --> 00:02:58,000
<i>Was it the school counselor?
Was it Ms. Crabtree?</i>

41
00:02:58,178 --> 00:03:00,976
<i>- Or was it-?
- I didn't find out who my father was!</i>

42
00:03:01,147 --> 00:03:05,174
<i>- Or was it Mrs. Broflovski?
- Wait, he's breathing. He's not dead.</i>

43
00:03:05,351 --> 00:03:08,115
- Who's my father?
- We have to get him to the hospital.

44
00:03:08,288 --> 00:03:12,122
- You've got to be kidding me!
- Come on, children.

45
00:03:12,292 --> 00:03:16,729
That poor kid. It must be hell
for him going through all this.

46
00:03:18,031 --> 00:03:22,468
There's a murderer in South Park! We have
to find who it is before they kill again!

47
00:03:22,635 --> 00:03:24,967
God only knows who they'll kill next.

48
00:03:25,138 --> 00:03:26,730
<i>Who will they kill next?</i>

49
00:03:26,906 --> 00:03:30,307
<i>Will it be Jimbo?
Barbrady? The Denver Broncos?</i>

50
00:03:32,111 --> 00:03:34,341
- Is he awake yet?
- He's bleeding bad.

51
00:03:34,514 --> 00:03:37,449
Don't let him bleed on my
Meredith Baxter Birney towel!

52
00:03:37,617 --> 00:03:41,986
- A Meredith Baxter Birney towel?
- I was with Meredith Baxter Birney in this car.

53
00:03:42,155 --> 00:03:46,489
Afterwards we used that towel to-
Wait, why am I telling you this?

54
00:03:46,659 --> 00:03:49,856
- Could you pull over?
- We have to get to the hospital.

55
00:03:50,029 --> 00:03:52,589
I have to get out.
I can't stand hospitals.

56
00:03:52,899 --> 00:03:56,528
We gotta drop Mephisto off.
Then we can get out of there.

57
00:03:56,703 --> 00:03:59,570
It's really snowing!
I hope they don't close the roads.

58
00:03:59,739 --> 00:04:01,673
They can't. Mephisto can't die.

59
00:04:01,841 --> 00:04:03,672
Maybe it's better you don't know
who your father is.

60
00:04:03,843 --> 00:04:06,744
No way. I can't stand
to leave things unfinished.

61
00:04:06,913 --> 00:04:10,405
Like when you hear the first part
of the song "Come Sail Away."

62
00:04:10,583 --> 00:04:12,983
If I hear that, I have to finish it.

63
00:04:13,152 --> 00:04:16,918
- Really?
- Yeah, I can't do anything until it's done.

64
00:04:21,628 --> 00:04:22,617
No, don't!

65
00:04:59,299 --> 00:05:00,288
Whoa, dude.

66
00:05:00,466 --> 00:05:02,991
What are we gonna do, Mayor?
A killer is on the loose.

67
00:05:03,169 --> 00:05:07,606
We can't even leave our homes
for fear of our children's safety!

68
00:05:07,774 --> 00:05:10,868
Where are our children?

69
00:05:11,044 --> 00:05:15,913
Officer Barbrady, let's pretend we had
a competent law enforcer in this town.

70
00:05:16,082 --> 00:05:17,879
What would he do?

71
00:05:18,051 --> 00:05:20,645
That's a good question, Mayor.

72
00:05:20,820 --> 00:05:24,051
Let me get right on that with thinking.

73
00:05:24,891 --> 00:05:26,483
Mayor, Mayor!

74
00:05:26,659 --> 00:05:29,355
- The press is here.
- My name is Sid Greenfield.

75
00:05:29,529 --> 00:05:32,362
<i>I'm the director from I.A.
for America's Most Wanted.</i>

76
00:05:32,532 --> 00:05:36,935
- You certainly made it here quickly.
- We're desperate for stories.

77
00:05:37,103 --> 00:05:40,095
- And this one is so compelling.
- Really?

78
00:05:40,273 --> 00:05:44,004
Sure, this story has everything.
People. Furniture. Talking.

79
00:05:44,177 --> 00:05:47,635
- It's a real American story.
- I thought of something.

80
00:05:47,814 --> 00:05:50,282
No, wait, that's subtraction.

81
00:05:50,450 --> 00:05:53,476
Mr. Director-person,
what do you want to do?

82
00:05:53,653 --> 00:05:58,056
- Mayor, shouldn't we be focusing-?
- We want to do a re-creation for our show.

83
00:05:58,224 --> 00:06:03,491
Then we'll flash a number people can call
if they have information about the shooter.

84
00:06:03,663 --> 00:06:06,427
That's it. You win, we win.
America wins.

85
00:06:06,599 --> 00:06:09,659
This won't make
our town look dangerous?

86
00:06:09,836 --> 00:06:14,796
<i>America's Most Wanted is not
about violence, it's about family.</i>

87
00:06:14,974 --> 00:06:18,705
- It is?
- Well, in that case, I guess it's okay.

88
00:06:18,878 --> 00:06:22,837
We'll get started with auditions.
What part should we cast first?

89
00:06:23,015 --> 00:06:25,313
<i>Who will the director cast first?</i>

90
00:06:25,485 --> 00:06:29,046
<i>Will it be Mr. Garrison?
Officer Barbrady? Chef?</i>

91
00:06:31,157 --> 00:06:33,591
- I want an abortion.
- We can do that.

92
00:06:33,760 --> 00:06:36,320
This must be a difficult time, Mrs...?

93
00:06:36,496 --> 00:06:38,862
Cartman. It's such a hard decision...

94
00:06:39,031 --> 00:06:43,331
...but I don't feel like I can
raise a child in this screwy world.

95
00:06:43,503 --> 00:06:47,633
If you don't feel fit to raise a child,
an abortion is the answer.

96
00:06:47,807 --> 00:06:52,369
- Do you know the time of conception?
- About eight years ago.

97
00:06:53,846 --> 00:06:57,304
- I see, so the fetus is...?
- 8 years old.

98
00:06:57,483 --> 00:07:01,510
Eight years old is a little late
to be considering abortion.

99
00:07:01,687 --> 00:07:02,676
Really?

100
00:07:02,855 --> 00:07:06,222
Yes, this is what we refer
to as the 40th trimester.

101
00:07:06,392 --> 00:07:11,659
- But I just don't think I'm a fit mother.
- We prefer to abort babies earlier on.

102
00:07:11,831 --> 00:07:14,959
There's a law against abortions
after the second trimester.

103
00:07:15,134 --> 00:07:19,594
Well, I think you need to keep
your laws off of my body!

104
00:07:19,772 --> 00:07:24,106
If you want to change the law you'll
have to speak with your congressman.

105
00:07:24,277 --> 00:07:27,872
Well, that's exactly what
I intend to do. Good day!

106
00:07:29,615 --> 00:07:33,142
- Come on, we gotta find a doctor.
- I can't do it.

107
00:07:33,319 --> 00:07:36,652
- Hospitals aren't that bad.
- Yeah, stop being a wuss.

108
00:07:37,723 --> 00:07:39,247
- Doctor!
- One moment.

109
00:07:39,425 --> 00:07:43,156
Nurse, I need 20 cc's
of sodium pentothal, stat!

110
00:07:43,329 --> 00:07:47,663
- Whoa, she doesn't have arms.
- We're an equal opportunity employer.

111
00:07:47,834 --> 00:07:50,064
We've got a shot cracker outside.

112
00:07:50,236 --> 00:07:54,070
- I'll be with you after I inject this man.
- I'm gonna be sick.

113
00:07:54,240 --> 00:07:57,767
There, there. Medical science
is nothing to be afraid of.

114
00:07:57,944 --> 00:08:00,310
I think you're hitting the bone.

115
00:08:01,247 --> 00:08:04,842
I can hear the needle
scraping against the bone inside.

116
00:08:05,017 --> 00:08:06,712
Oops, he's hemorrhaging.

117
00:08:07,587 --> 00:08:10,055
- His head fell off.
- I'm getting out of here.

118
00:08:10,223 --> 00:08:12,418
- Stan!
- Some people have a weak stomach.

119
00:08:15,061 --> 00:08:16,995
The father of Eric Cartman is-

120
00:08:17,163 --> 00:08:19,631
Bang!
Jiminy, I've been shot.

121
00:08:19,799 --> 00:08:23,565
We'll get back to you.
I've seen enough genetic engineers.

122
00:08:23,736 --> 00:08:27,228
Let's move on to the auditions
for Mr. Garrison.

123
00:08:27,406 --> 00:08:29,465
Call the Mr. Garrison auditions.

124
00:08:29,642 --> 00:08:33,078
I sure hope I'm not
Eric Cartman's father, Mr. Hat.

125
00:08:33,246 --> 00:08:39,151
- You can say that again, Mr. Garrison.
- Let's keep him on the top pile. Next!

126
00:08:39,318 --> 00:08:42,219
I sure hope I'm not
Eric Cartman's father, Mr. Hat.

127
00:08:42,388 --> 00:08:44,652
You can say that again, Mr. Garrison.

128
00:08:44,824 --> 00:08:46,951
- Thank you. Next!
- What?!

129
00:08:47,126 --> 00:08:48,218
Next!

130
00:08:49,195 --> 00:08:51,857
I hope I'm not
Eric Cartman's father, Mr. Hat.

131
00:08:52,031 --> 00:08:55,194
- You can say that again, Mr. Garrison.
- Perfect! You got it.

132
00:08:55,368 --> 00:08:57,097
Let's move on to the Chefs.

133
00:08:57,270 --> 00:08:59,295
So you see, Congressman O'Reilly...

134
00:08:59,472 --> 00:09:02,669
...that's why I think abortion laws
should be changed.

135
00:09:02,842 --> 00:09:07,711
I know third trimester abortions are illegal.
I don't know anything about 40th.

136
00:09:07,880 --> 00:09:09,780
The person at Unplanned Parenthood...

137
00:09:09,949 --> 00:09:13,180
...said you were who I had to talk
to about changing the law.

138
00:09:13,352 --> 00:09:16,082
I think you've got to talk
to the governor about that.

139
00:09:16,255 --> 00:09:17,381
Oh, dear.

140
00:09:18,724 --> 00:09:22,626
- This is all I can do for him.
- I have to know who my father is!

141
00:09:22,795 --> 00:09:26,424
- Sorry, it might be a while.
- Wake up, you son of a bitch!

142
00:09:26,599 --> 00:09:30,057
That's not gonna do any good.
I'll let you know if there's a change.

143
00:09:30,236 --> 00:09:33,296
- I can't wait. What am I supposed to do?
- Hey, Cartman!

144
00:09:33,472 --> 00:09:34,461
What?

145
00:09:43,583 --> 00:09:46,882
- Let's get out of here.
- I don't know if that's gonna be easy.

146
00:09:47,053 --> 00:09:49,317
- That's a hell of a storm.
- Weak.

147
00:09:53,926 --> 00:09:57,828
I hate mountains. This better
not push back our shooting schedule.

148
00:09:57,997 --> 00:10:01,626
Let's rehearse the reenactment
from the top before we shoot it.

149
00:10:01,801 --> 00:10:04,167
- Who's that?
- That's TV's Eric Roberts.

150
00:10:04,337 --> 00:10:07,568
We were able to get him
to play the little monkey guy.

151
00:10:07,740 --> 00:10:10,766
Talk about washed up.
Great to have you, Eric!

152
00:10:12,011 --> 00:10:13,672
Here we go. And, action!

153
00:10:13,846 --> 00:10:16,781
- I want to announce who the father is.
- Bang.

154
00:10:16,949 --> 00:10:18,246
I've been shot.

155
00:10:18,417 --> 00:10:21,944
- Nobody move. I'm a law officer.
- He's gone into cardiac arrest.

156
00:10:22,121 --> 00:10:24,248
You seem unnerved by this, Chef.

157
00:10:24,423 --> 00:10:28,826
Are you accusing me? If you are,
don't hide behind your clever riddles!

158
00:10:28,995 --> 00:10:31,122
Our differences must be set aside.

159
00:10:31,297 --> 00:10:34,164
I'm simply a man.
A man trying to do my job.

160
00:10:34,333 --> 00:10:36,494
Wow, this is a good movie.

161
00:10:45,544 --> 00:10:47,273
When can we get out of here?

162
00:10:47,446 --> 00:10:50,506
- Doctor, I can't focus.
- We're doing the best we can.

163
00:10:50,683 --> 00:10:52,844
They've closed the pass
and no doctors can get through.

164
00:10:53,019 --> 00:10:57,752
- For now, it's me and Nurse Goodly.
- Wait a second, they've closed the pass?

165
00:10:57,923 --> 00:11:01,984
Yes, we're critically understaffed.
Unless we get help...

166
00:11:02,161 --> 00:11:04,925
...these people are completely f**ked.

167
00:11:05,097 --> 00:11:07,657
- Metaphorically speaking.
- What about Mephisto?

168
00:11:07,833 --> 00:11:10,768
- Are you taking care of him?
- He's on life support.

169
00:11:10,936 --> 00:11:15,839
He shouldn't need any help.
So long as the power doesn't go out.

170
00:11:16,008 --> 00:11:18,875
Who didn't see that coming
a mile away, huh?

171
00:11:20,346 --> 00:11:21,870
<i>Tonight on America's Most Wanted...</i>

172
00:11:22,048 --> 00:11:25,449
<i>...a shooter is loose in Colorado,
and the residents are up in arms!</i>

173
00:11:25,618 --> 00:11:27,210
<i>Here's your host, John Walsh.</i>

174
00:11:27,386 --> 00:11:29,377
<i>Welcome to America's Most Wanted.</i>

175
00:11:29,555 --> 00:11:32,888
Tonight, terror invaded
South Park, Colorado...

176
00:11:33,059 --> 00:11:36,153
...when Dr. Mephisto was
gunned down in his laboratory.

177
00:11:36,328 --> 00:11:38,956
<i>America's Most Wanted
has reconstructed this crime...</i>

178
00:11:39,131 --> 00:11:41,531
...in hopes your calls
can help solve the case.

179
00:11:42,868 --> 00:11:45,302
- I've been shot.
- Mr. Hat, get some help!

180
00:11:45,471 --> 00:11:47,200
Right away, Mr. Garrison!

181
00:11:47,373 --> 00:11:52,504
No! My lifetime partner and friend, taken
away in the wink of an eye. Fortune...

182
00:11:52,678 --> 00:11:55,545
What the-?
Eric Roberts is improvising lines again.

183
00:11:55,715 --> 00:11:58,047
I'm gonna get him to a hospital.
Who will help?

184
00:11:58,217 --> 00:12:01,243
This is not a world I want to live in.
It is an angry-

185
00:12:03,956 --> 00:12:07,119
- What the hell is this?
- We've lost the feed to I.A.

186
00:12:07,293 --> 00:12:11,229
We seem to have lost our linkup
to the South Park crew.

187
00:12:11,397 --> 00:12:15,731
<i>I guess we'll be going to our feature
movie, Who Framed Roger Rabbit.</i>

188
00:12:15,901 --> 00:12:20,634
<i>Who framed Roger Rabbit?
Was it Jimbo? Mr. Garrison? Chef?</i>

189
00:12:20,806 --> 00:12:23,570
Somebody go see
why we lost the hookup.

190
00:12:23,743 --> 00:12:26,712
Holy smokes!
That blizzard is getting crazy.

191
00:12:30,149 --> 00:12:35,416
Don't panic. The power lines are down
but the backup generator is running fine.

192
00:12:35,588 --> 00:12:38,216
- Nurse, I could use some help.
- Coming.

193
00:12:38,390 --> 00:12:40,551
Lady, is Mephisto going to be okay?

194
00:12:40,726 --> 00:12:44,218
Yes, for now.
But the generator won't run for long.

195
00:12:44,396 --> 00:12:47,923
The batteries run out in half an hour.
Time is very short.

196
00:12:48,100 --> 00:12:51,501
Nurse, please!
I need another pair of hands in here.

197
00:12:51,670 --> 00:12:53,399
Sorry.

198
00:12:53,572 --> 00:12:58,600
Don't you see, governor? I should have
a right to have an abortion if I want.

199
00:12:58,778 --> 00:13:02,441
Well, I don't know.
I might need some more convincing.

200
00:13:02,615 --> 00:13:07,643
What right do I have bringing another
child into this overpopulated world?

201
00:13:07,820 --> 00:13:12,223
Then again, I should have thought
of that before having sex. I don't know.

202
00:13:12,391 --> 00:13:16,384
I have 100 people to tend to,
and only myself and Nurse Goodly.

203
00:13:16,562 --> 00:13:17,551
What do you want me to do?

204
00:13:17,730 --> 00:13:20,528
<i>- Do you know about surgery?
- I used to watch Quincy.</i>

205
00:13:20,699 --> 00:13:25,727
Why didn't you say so? Put on scrubs!
Boys, I'm making you honorary doctors.

206
00:13:25,905 --> 00:13:28,635
- You can help us save lives.
- No way, dude!

207
00:13:31,210 --> 00:13:34,077
We're not going anywhere
for a long time.

208
00:13:34,246 --> 00:13:37,113
- We're snowed in?
- Yes, we're trapped.

209
00:13:37,283 --> 00:13:39,478
Like sailors on a submarine.

210
00:13:39,652 --> 00:13:42,780
My God, this is the worst storm
I've ever seen.

211
00:13:42,955 --> 00:13:46,083
I have to get out.
I haven't eaten since breakfast.

212
00:13:46,258 --> 00:13:48,021
I'm getting hungry too.

213
00:13:48,194 --> 00:13:51,288
I hope you all realize
what we might be facing.

214
00:13:51,463 --> 00:13:56,867
Our only option might be
to eat each other to stay alive.

215
00:13:57,036 --> 00:14:02,064
It's only been four hours. Aren't you
resorting to cannibalism quickly?

216
00:14:02,241 --> 00:14:03,765
That's the law of land.

217
00:14:03,943 --> 00:14:08,073
It ain't pretty, but if a few must die
so the rest can live, so be it.

218
00:14:08,247 --> 00:14:11,876
- But how do we decide who?
- Well, we draw straws.

219
00:14:12,051 --> 00:14:16,351
We all had a big breakfast.
Can't you go without eating for a while?

220
00:14:16,522 --> 00:14:21,323
Calm down! We need every person to keep
his head. Barbrady, fetch some straws.

221
00:14:21,493 --> 00:14:23,825
Who the hell made you the boss?

222
00:14:23,996 --> 00:14:28,899
<i>Who the hell made Jimbo boss?
Was it Barbrady? Chef? Mr. Gar-</i>

223
00:14:30,069 --> 00:14:33,596
His appendix has burst!
I have to operate with our limited power.

224
00:14:33,772 --> 00:14:37,868
I need you to be strong. Nurse Goodly
will take care of the anesthesia.

225
00:14:38,043 --> 00:14:41,240
Chef, act as her arms.
Boys, help with suction and bandages.

226
00:14:41,413 --> 00:14:42,402
- Ready?
- No!

227
00:14:42,581 --> 00:14:47,018
- I'll make an incision over the heart.
- Oh, boy.

228
00:14:51,657 --> 00:14:54,148
- You barfed into the incision!
- Sweet!

229
00:14:54,326 --> 00:14:56,055
Suction!

230
00:14:58,063 --> 00:15:02,056
<i>- They're screwing with the lights!
- Who is screwing with the lights?</i>

231
00:15:02,234 --> 00:15:03,997
<i>Is it Barbrady? Or Jimbo?</i>

232
00:15:04,169 --> 00:15:08,367
<i>- Or the 1991 Denver Broncos?
- That is really starting to piss me off.</i>

233
00:15:10,442 --> 00:15:15,675
So far, everybody has a long
piece of straw. We'll keep drawing.

234
00:15:19,585 --> 00:15:23,248
- Wait. Where the hell is the short one?
- The short what?

235
00:15:23,422 --> 00:15:27,085
When you draw straws, you're
supposed to have one of them short!

236
00:15:27,259 --> 00:15:30,695
- That's how you decide who loses.
- That's not how I played.

237
00:15:30,863 --> 00:15:34,299
Could we hurry this up?
My stomach is growling.

238
00:15:35,968 --> 00:15:38,562
I found a map that shows
the location of a backup generator.

239
00:15:38,737 --> 00:15:43,003
They built a self-sustaining generator
for this kind of emergency.

240
00:15:43,175 --> 00:15:45,700
- It's away from the hospital.
- How do we get to it?

241
00:15:45,878 --> 00:15:48,847
We split up into two teams.
Team A and Team B.

242
00:15:49,014 --> 00:15:53,508
Team A will consist of myself, Stan,
Kyle, Eric, Chef and Nurse Goodly.

243
00:15:53,686 --> 00:15:56,086
Team B will consist of Kenny.

244
00:15:56,255 --> 00:15:59,747
Team B, your goal will be
to turn on the backup generator.

245
00:15:59,925 --> 00:16:03,292
To do this, you must
get into this sewage duct.

246
00:16:03,462 --> 00:16:07,728
Team A will go to the holding area
where there is a television and cocoa.

247
00:16:07,900 --> 00:16:12,200
We will drink cocoa and watch TV
until Team B makes it through the duct.

248
00:16:12,371 --> 00:16:14,771
By that time, Team B-
That's you, Kenny...

249
00:16:14,940 --> 00:16:17,306
...should reach the outer core
of the generator.

250
00:16:17,476 --> 00:16:20,502
It will be a dangerous climb.
There could be velociraptors.

251
00:16:20,679 --> 00:16:25,742
Once you reach the top, you will see us
through this window watching TV.

252
00:16:25,918 --> 00:16:29,285
Then you can proceed
into the generator and power it on.

253
00:16:29,455 --> 00:16:32,515
- Any questions?
- No, that sounds pretty sweet to me.

254
00:16:32,691 --> 00:16:35,216
Right, then let's do it. Go team!

255
00:16:37,696 --> 00:16:40,290
I can't go on.

256
00:16:40,466 --> 00:16:42,400
So hungry.

257
00:16:42,568 --> 00:16:46,004
We're all going to die
in this horrible place.

258
00:16:46,171 --> 00:16:50,198
We have to have energy to make
it through the night. We have to eat.

259
00:16:50,376 --> 00:16:51,365
How can we?

260
00:16:51,543 --> 00:16:57,504
- How could we live with ourselves?
- There's only one answer. Eat Eric Roberts!

261
00:16:57,683 --> 00:17:01,483
Yes, of course.
Nobody gives as **t about Eric Roberts.

262
00:17:01,653 --> 00:17:03,177
Eat Eric Roberts!

263
00:17:04,523 --> 00:17:06,582
No! No, please!

264
00:17:10,462 --> 00:17:16,230
Well, there's no going back now.
We're cannibals. God save us.

265
00:17:16,402 --> 00:17:18,597
God wants you to live. Fight.

266
00:17:18,771 --> 00:17:22,798
I have to admit, Eric Roberts
was much juicier than I expected.

267
00:17:22,975 --> 00:17:26,467
This snow isn't letting up.
We're gonna die here, I know it!

268
00:17:26,645 --> 00:17:30,240
- What time is it, Barbrady?
- It's almost midnight.

269
00:17:30,416 --> 00:17:32,475
I can't go on.

270
00:17:32,651 --> 00:17:36,451
We'll give the storm an hour.
After that we might have to eat again.

271
00:17:36,622 --> 00:17:39,284
Christ! Are you people
diabetic or something?

272
00:17:42,861 --> 00:17:46,319
<i>- Team B? Come in, Team B.
- This is Team B.</i>

273
00:17:46,498 --> 00:17:49,126
We found another path to the generator.

274
00:17:49,301 --> 00:17:54,432
There's a nice heated walkway to it,
so you don't need to walk through sewage.

275
00:17:57,743 --> 00:17:59,734
Well, forget I said that then.

276
00:17:59,912 --> 00:18:03,643
You should see a large drift of snow
with metal sticking out of it.

277
00:18:06,485 --> 00:18:09,477
Good, head towards it.
Team A out.

278
00:18:09,655 --> 00:18:13,489
What if Mephisto never wakes up?
And I never find out who my father is?

279
00:18:21,066 --> 00:18:24,593
My God, what a harrowing tale
of human drama this is!

280
00:18:24,770 --> 00:18:27,762
Doing what we must to survive.

281
00:18:27,940 --> 00:18:30,067
It is amazing what people
do under stress.

282
00:18:30,242 --> 00:18:34,235
Look at the pyramids. Nobody knows
how they built those, or who.

283
00:18:34,413 --> 00:18:36,574
<i>Who built the pyramids?</i>

284
00:18:36,748 --> 00:18:40,184
<i>Was it the Babylonians?
Officer Barbrady? Samaritans?</i>

285
00:18:42,454 --> 00:18:44,945
Roger, Team B.
He's reached the backup generator.

286
00:18:45,124 --> 00:18:48,651
Team B, can you see
the two copper nodes? Good.

287
00:18:48,827 --> 00:18:52,661
Now, is there a wire connecting them?

288
00:18:52,831 --> 00:18:57,291
The wire connecting the nodes is gone.
We need to complete the circuit.

289
00:18:57,469 --> 00:18:59,198
- Do you have any wire?
- There's no time.

290
00:18:59,371 --> 00:19:03,501
Once these lights flicker out,
the patients on life support will die.

291
00:19:05,077 --> 00:19:08,740
You can't. There must be some other way.
He's gonna make the connection...

292
00:19:08,914 --> 00:19:10,814
- ...with his hands.
- He'll die!

293
00:19:10,983 --> 00:19:13,952
- Go, Kenny!
- Kenny, no!

294
00:19:17,189 --> 00:19:20,556
- Oh, my God! They've killed Kenny.
- You bastards!

295
00:19:21,760 --> 00:19:25,389
- The power!
- Quickly, get the scanner running again.

296
00:19:25,564 --> 00:19:26,895
We've got a chance now!

297
00:19:28,967 --> 00:19:34,269
Well, okay, Mrs. Cartman. I'll legalize
40th trimester abortions for you.

298
00:19:34,439 --> 00:19:35,736
Thank you, thank you!

299
00:19:35,908 --> 00:19:38,570
We'll have
the pregnancy terminated.

300
00:19:38,744 --> 00:19:41,736
- Terminated?
- Yes, that's what an abortion is.

301
00:19:41,914 --> 00:19:45,714
I didn't mean that.
I meant the other thing you can do.

302
00:19:45,884 --> 00:19:48,614
- What's that other A word?
- Adoption?

303
00:19:48,787 --> 00:19:53,121
- Yes, that's what I mean. Adoption.
- That's pretty different.

304
00:19:53,292 --> 00:19:57,695
I should tell my son the truth about
everything. Good day, Mr. President.

305
00:20:01,967 --> 00:20:05,266
We made it. The power is on,
the snow is melting...

306
00:20:05,437 --> 00:20:07,337
...and Mephisto is doing fine.

307
00:20:07,506 --> 00:20:11,033
- Where am I?
- You're at the hospital, Mr. Mephisto.

308
00:20:11,210 --> 00:20:15,010
You were shot. We don't know
who tried to shoot you, but-

309
00:20:15,180 --> 00:20:18,741
I'm sure it was my brother.
He tries to shoot me every month.

310
00:20:18,917 --> 00:20:21,784
Where's the fat boy? He'll be
delighted that Mephisto is awake.

311
00:20:21,954 --> 00:20:23,216
He's looking for Kenny.

312
00:20:23,388 --> 00:20:28,451
- He was a good friend, and I'll miss him.
- He risked his life so Mephisto could live.

313
00:20:28,627 --> 00:20:31,562
Yeah, and now he's a freezy pop.

314
00:20:33,165 --> 00:20:36,931
- Do you think if we hit him, he'd shatter?
- Let's find out!

315
00:20:44,076 --> 00:20:48,979
- It's over. We're free.
- Yes, but at what cost, Mr. Garrison?

316
00:20:49,147 --> 00:20:50,273
At what cost?

317
00:20:50,449 --> 00:20:53,213
Listen, we did what we had to in there.

318
00:20:53,385 --> 00:20:57,253
But how will we live with ourselves now?

319
00:20:57,422 --> 00:21:02,018
One day at a time, Mayor.
One day at a time.

320
00:21:11,770 --> 00:21:15,604
I'm bringing home some Eric Roberts.
Anybody else want some?

321
00:21:17,042 --> 00:21:21,308
I can finally reveal
who the father of Eric Cartman is.

322
00:21:21,480 --> 00:21:24,847
First, I want to thank Kenny
McCormick for sacrificing-

323
00:21:25,017 --> 00:21:26,951
- Tell us, already!
- All right.

324
00:21:27,119 --> 00:21:29,053
The father of Eric Cartman is-

325
00:21:29,221 --> 00:21:31,917
<i>Did anybody see that
Terrance & Phillip Special last month?</i>

326
00:21:32,090 --> 00:21:35,582
- Wasn't that just the funniest-?
- Tell me who my father is!

327
00:21:35,761 --> 00:21:39,720
As I said before,
the father is somebody in this room.

328
00:21:39,898 --> 00:21:41,832
The father is...

329
00:21:42,000 --> 00:21:43,900
...Mrs. Cartman.

330
00:21:44,069 --> 00:21:46,264
- What?
- Yes, it's true.

331
00:21:46,438 --> 00:21:48,235
No, that doesn't make sense.

332
00:21:48,407 --> 00:21:51,240
It took a while for me
to understand as well.

333
00:21:51,410 --> 00:21:55,346
You see, Mrs. Cartman
is a hermaphrodite.

334
00:21:55,514 --> 00:21:56,606
Meaning what?

335
00:21:56,782 --> 00:22:00,274
Meaning that she has
both male and female genitals.

336
00:22:00,452 --> 00:22:01,476
It's true.

337
00:22:01,653 --> 00:22:06,090
You mean at the Drunken Barn Dance
when we all got together with her...

338
00:22:06,258 --> 00:22:07,316
...she was a he?

339
00:22:07,492 --> 00:22:11,223
No, no, not exactly.
But she did have a penis.

340
00:22:11,830 --> 00:22:16,961
Hermaphrodites cannot bear children,
so Mrs. Cartman's DNA match with Eric...

341
00:22:17,135 --> 00:22:19,330
...means that she is his father...

342
00:22:19,504 --> 00:22:23,304
...and she got a woman pregnant
at the Drunken Barn Dance.

343
00:22:23,475 --> 00:22:25,204
Man, this is f**king weak.

344
00:22:25,377 --> 00:22:28,278
You're a fat-ass,
and your mom's a "hermaphalite."

345
00:22:28,447 --> 00:22:33,783
I'm sorry I never told you, Eric. I just
thought it would be a little shocking.

346
00:22:33,952 --> 00:22:35,715
Wow. You think so, Mom?

347
00:22:35,887 --> 00:22:39,653
- That's that. Thank you all for playing.
- Wait a minute.

348
00:22:39,825 --> 00:22:42,521
If she's my dad, then who's my mom?

349
00:22:42,694 --> 00:22:45,219
<i>Who is Eric Cartman's mother?</i>

350
00:22:45,397 --> 00:22:48,298
<i>Is it Mrs. Crabtree?
Sheila Broflovski? The Mayor?</i>

351
00:22:48,467 --> 00:22:50,025
Forget it!
