require 'lingua/stemmer'
require 'uea-stemmer'
require 'engtagger'
require 'Linguistics'
require 'srt'
require 'pg'

def cleaner(string)
  string.gsub(/(<i>)|(<\/i>)|[^\w' ]|\d/, '')
end

def add_to_trash(array)
  array.each do |word|
  if $conn.query("SELECT id FROM trash WHERE word = '#{encode(word)}'").values.empty?
    $conn.query("INSERT INTO trash (word) VALUES ('#{encode(word.downcase)}')")
    puts "'#{word.downcase}' ---- was added to 'trash' table"
  end
  end
end

def encode(word)
  word.gsub(/'/, "''")
end

tgr = EngTagger.new
stemmer = UEAStemmer.new

$conn = PG.connect :dbname => 'english', :user => 'postgres', :password => 'password'

puts "Please put *.srt file"
srt_name = gets.chomp

file = SRT::File.parse(File.new("/Users/anton/Documents/projects/english/srt/#{srt_name}"))

strings = []
file.lines.each { |line| strings.push(line.text.join(' ')) }

words = strings.join(' ').split(' ')
words.map! { |word| cleaner(word) }
words.uniq!
words.reject! {|e| e.empty?}

#create one large string
text = words.join(' ')
tagged = tgr.add_tags(text)

#get an array of verbs
verb = tagged.scan(/\<vb>\w+\<\/vb>|\<vbd>\w+\<\/vbd>|\<vbg>\w+\<\/vbg>|\<vbn>\w+\<\/vbn>|\<vbp>\w+\<\/vbp>|\<vbz>\w+\<\/vbz>|
/).map! {|word| word.gsub(/\<vb>|\<\/vb>|\<vbd>|\<\/vbd>|\<vbg>|\<\/vbg>|\<vbn>|\<\/vbn>|\<vbp>|\<\/vbp>|\<vbz>|\<\/vbz>/, '')}

#Adverb
adverb = tagged.scan(/\<rbr>\w+\<\/rbr>|\<rb>\w+\<\/rb>|\<rbs>\w+\<\/rbs>|\<rp>\w+\<\/rp>/).map! {|word| word.gsub(/\<rbr>|<\/rbr>|\<rb>|\<\/rb>|\<rbs>|\<\/rbs>|\<rp>\|\<\/rp>/, '')}

#Adjective
adj = tgr.get_adjectives(tagged).keys

#Noun
nouns = tgr.get_nouns(tagged).keys

selected_words = verb + adverb + adj + nouns
#ta = selected_words.map {|word| stemmer.stem(word).downcase}.uniq

lingualeo_dict = $conn.query("SELECT word FROM dict").values.flatten
trash = $conn.query("SELECT word FROM trash").values.flatten

sum = lingualeo_dict + trash

selected_words.delete_if { |word| lingualeo_dict.include?(word.downcase) || word.length < 4}.sort_by {|word| word.length}
#ta.each_with_index {|word, index| puts "#{index+1}. #{word}"}
results = selected_words.sort_by {|word| word.length}
results.uniq!
puts "#{results.count}"
results.each { |word| puts word }
