require 'open-uri'
require 'nokogiri'
require 'net/http'
require 'pg'

$conn = PG.connect :dbname => 'english', :user => 'postgres', :password => 'password'

source = File.open("/Users/anton/Desktop/lingualeo.htm", "r")

page = Nokogiri::HTML(source)

def encoded(string)
  string.gsub(/'/, "''")
end

def checker(word, translation)
  result = $conn.query ( "SELECT id FROM dict WHERE word = '#{encoded(word)}'" )
  if result.count == 0
    $conn.query ( "INSERT INTO dict (word, translation) VALUES ('#{encoded(word)}', '#{translation}')" )
    puts "#{word} has added to DB"
  end
end

DB_size_start = $conn.query ( "SELECT id FROM dict" )

page.xpath('//*[@id="word-table"]/tbody/tr').each do |row|
  word = row.css('td')[1].text.gsub(/^ | {2,}/,'')
  translation = row.css('td')[3].text.gsub(/^ | {2,}/,'')
  checker(word, translation)
end

DB_size_finish = $conn.query ( "SELECT id FROM dict" )

puts "#{DB_size_finish.count-DB_size_start.count} added to database"
