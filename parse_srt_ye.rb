require 'uea-stemmer'
require 'engtagger'
require 'srt'
require 'nokogiri'

def cleaner(string)
  string.gsub(/(<i>)|(<\/i>)|[^\w' ]|\d/, '')
end

tgr = EngTagger.new
stemmer = UEAStemmer.new

puts "Please put name of *.srt file"
srt_name = gets.chomp

#change path to file
file = SRT::File.parse(File.new("/Users/anton/Desktop/test/#{srt_name}"))

#change path to file
#lingualeo.htm should be changed
source = File.open("/Users/anton/Desktop/test/lingualeo.htm", "r")
page = Nokogiri::HTML(source)

word_array = []

page.xpath("//*[@id='word-table']/tbody/tr").each do |row|
  hash = {}
  hash = {
    word: row.css('td')[1].text.gsub(/^ | {2,}/,''),
    translation: row.css('td')[3].text.gsub(/^ | {2,}/,'')
  }
  word_array.push(hash)
end

strings = []
file.lines.each { |line| strings.push(line.text.join(' ')) }

words = strings.join(' ').split(' ')
words.map! { |word| cleaner(word) }
words.uniq!
words.reject! {|e| e.empty?}

#create one large string
text = words.join(' ')
tagged = tgr.add_tags(text)

#get an array of verbs
verb = tagged.scan(/\<vb>\w+\<\/vb>|\<vbd>\w+\<\/vbd>|\<vbg>\w+\<\/vbg>|\<vbn>\w+\<\/vbn>|\<vbp>\w+\<\/vbp>|\<vbz>\w+\<\/vbz>|
/).map! {|word| word.gsub(/\<vb>|\<\/vb>|\<vbd>|\<\/vbd>|\<vbg>|\<\/vbg>|\<vbn>|\<\/vbn>|\<vbp>|\<\/vbp>|\<vbz>|\<\/vbz>/, '')}

#Adverb
adverb = tagged.scan(/\<rbr>\w+\<\/rbr>|\<rb>\w+\<\/rb>|\<rbs>\w+\<\/rbs>|\<rp>\w+\<\/rp>/).map! {|word| word.gsub(/\<rbr>|<\/rbr>|\<rb>|\<\/rb>|\<rbs>|\<\/rbs>|\<rp>\|\<\/rp>/, '')}

#Adjective
adj = tgr.get_adjectives(tagged).keys

#Noun
nouns = tgr.get_nouns(tagged).keys

selected_words = verb + adverb + adj + nouns
#ta = selected_words.map {|word| stemmer.stem(word).downcase}.uniq

lingualeo_dict = word_array.map {|x| x.values[1]}

selected_words.delete_if { |word| lingualeo_dict.include?(word.downcase) || word.length < 4}.sort_by {|word| word.length}
#ta.each_with_index {|word, index| puts "#{index+1}. #{word}"}
results = selected_words.sort_by {|word| word.length}
results.map! {|word| word.downcase}.uniq!
puts "#{results.count}"
results.each { |word| puts word }
